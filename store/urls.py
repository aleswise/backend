from django.contrib import admin
from django.urls import path

from api.views import LoginRetrieveAPIView, LoginListAPIView
from api.views import ClientCreateAPIView, ClientRetrieveAPIView, ClientListAPIView
from api.views import InvoiceRetrieveAPIView, InvoiceListAPIView
from api.views import ProductCreateAPIView, ProductRetrieveAPIView, ProductListAPIView, ProductUpdateAPIView, ProductDestroyAPIView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/<str:pk>', LoginRetrieveAPIView.as_view()),
    path('login/', LoginListAPIView.as_view()),
    path('clients/', ClientListAPIView.as_view()),
    path('clients/create', ClientCreateAPIView.as_view()),
    path('clients/<int:pk>', ClientRetrieveAPIView.as_view()),
    path('invoice/<int:pk>', InvoiceRetrieveAPIView.as_view()),
    path('invoices/', InvoiceListAPIView.as_view()),
    path('products/', ProductListAPIView.as_view()),
    path('product/<int:pk>', ProductRetrieveAPIView.as_view()),
    path('product/create', ProductCreateAPIView.as_view()),
    path('product/update/<int:pk>', ProductUpdateAPIView.as_view()),
    path('product/delete/<int:pk>', ProductDestroyAPIView.as_view()),
]
