from django.db import models

class Product(models.Model):
    nombre = models.CharField(max_length = 50)
    cantidad = models.IntegerField()
    descripcion = models.TextField()
    precio = models.DecimalField(max_digits= 15, decimal_places=2)