from django.db import models

from .clientmodel import Client

class Login(models.Model):
    username = models.CharField(max_length = 50, primary_key = True)
    password = models.CharField(max_length = 256)
    client = models.ForeignKey(Client, on_delete = models.CASCADE)