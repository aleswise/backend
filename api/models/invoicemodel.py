from django.db import models

from .clientmodel import Client

class Invoice(models.Model):
    date = models.DateField(auto_now_add = True)
    amount = models.DecimalField(max_digits = 15, decimal_places = 2)
    client = models.ForeignKey(Client, on_delete = models.CASCADE)