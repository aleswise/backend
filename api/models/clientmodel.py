from django.db import models

class Client(models.Model):
    fullname = models.CharField(max_length = 200)
    email = models.EmailField()