from rest_framework.serializers import ModelSerializer

from api.models import Invoice

class InvoiceSerializer(ModelSerializer):
    class Meta:
        model = Invoice
        fields = '__all__'

    def to_representation(self, instance):
        return {
            "date": instance.date,
            "amount": instance.amount,
            "client": {
                "name": instance.client.name,
                "address": instance.client.address,
                "cellphone": instance.client.cellphone
            }
        }