from rest_framework.serializers import ModelSerializer

from api.models import Client, Login
from .loginserializer import LoginSerializer

class ClientSerializer(ModelSerializer):
    login = LoginSerializer()
    class Meta:
        model = Client
        fields = ['fullname', 'email', 'login']

    def create(self, validated_data):
        new_login = validated_data.pop('login')
        new_client = Client.objects.create(**validated_data)
        Login.objects.create(**new_login, client = new_client)
        return new_client

    def to_representation(self, instance):
        client = Client.objects.get(id = instance.id)
        login = Login.objects.get(client = instance.id)
        return {
            "fullname": client.fullname,
            "email": client.email,
            "login": {
                "username": login.username,
                "password": login.password
            }
        }