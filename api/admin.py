from django.contrib import admin

from api.models import Login, Client, Invoice, Product

# Register your models here.
admin.site.register(Login)
admin.site.register(Client)
admin.site.register(Invoice)
admin.site.register(Product)