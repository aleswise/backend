from rest_framework import generics

from api.models import Invoice
from api.serializers import InvoiceSerializer

class InvoiceRetrieveAPIView(generics.RetrieveAPIView):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer

class InvoiceListAPIView(generics.ListAPIView):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer