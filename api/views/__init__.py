from .loginview import LoginRetrieveAPIView, LoginListAPIView
from .clientview import ClientCreateAPIView, ClientRetrieveAPIView, ClientListAPIView
from .invoiceview import InvoiceRetrieveAPIView, InvoiceListAPIView
from .productview import ProductCreateAPIView, ProductRetrieveAPIView, ProductListAPIView, ProductUpdateAPIView, ProductDestroyAPIView