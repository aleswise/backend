from rest_framework import generics

from api.models import Login
from api.serializers import LoginSerializer

class LoginRetrieveAPIView(generics.RetrieveAPIView):
    queryset = Login.objects.all()
    serializer_class = LoginSerializer

class LoginListAPIView(generics.ListAPIView):
    queryset = Login.objects.all()
    serializer_class = LoginSerializer